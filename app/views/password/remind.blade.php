@section('content')
	<!-- ........................................................................................... -->
<!-- CONTEÚDO -->
<div class="login">
 <img class="logo" src="{{ asset('img/logo-rsa-dark.png') }}">
 <div class="box">
 	@include('layouts.notifications')
    <!-- form recadastrar senha -->
    <h3>Recuperar senha</h3>
    {{ Form::open(['route'=>'password.request']) }}
       <div class="form-group">
         {{ Form::email('email', null, ["class"=>"form-control", "placeholder"=>"E-mail cadsatrado"]) }}
       </div>
       <button type="submit" class="pull-right">Recadastrar senha</button>
    {{ Form::close() }}
    <!-- /form recadastrar senha -->

    <a href="{{ route('home') }}" class="voltar color-pink">Voltar</a>
 </div>
</div>

@stop