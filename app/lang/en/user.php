<?php 

return array(

	'docError' => ':element inválido',
	'notfound' => 'Usuário não encontrado',
	'activated' => 'Usuário ativado com sucesso!',
	'notactivated' => 'Erro ao ativar usuário',
	'sendtofriend' => 'Enviado para o amigo com sucesso!',

	'register' => array(
		'success' => 'Usuário registrado com sucesso, agora acesse seu Email com as instruções para sua validação',
		'error' => 'Erro ao cadastrar usuário, tente novamente mais tarde',
		'exists' => 'Usuário já existente!',
		'required' => array(
			'login' => 'Email obrigatório',
			'pass' => 'Senha obrigatória'
		)
	),
	'edit' => array(
		'success' => 'Usuário editado com sucesso',
		'error' => 'Erro ao editar',
	)
);