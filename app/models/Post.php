<?php

class Post extends \Eloquent {

	protected $fillable = ['title','excerpt','content'];

	public static $updateRules = [
		'title' => 'required',
		'content'  => 'required',
		'excerpt'  => 'required',
	];
	public static $rules = [
		'title' => 'required',
		'content'  => 'required',
		'excerpt'  => 'required',
	];
	
	public function categories()
	{
		return $this->belongsToMany('Category', 'post_category');
	}

	public function franchises()
	{
		return $this->belongsToMany('Franchise', 'franchise_posts');
	}

	public static function boot()
	{
		parent::boot();

		static::deleted(function($post){
			
			
			
		});
	}

}