<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>RSA Franquias - Administrativo</title>
      <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
     
      {{ HTML::style('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css') }}
      {{ HTML::style('css/datatables.css') }}
      {{ HTML::style('css/font-awesome/css/font-awesome.min.css') }}
      {{ HTML::style('css/index.css') }}
      <!--[if lt IE 9]>
         {{ HTML::style('css/sequence-theme.two-up-ie8.css') }}
      <![endif]-->
   </head>
   <body class="admin">

      <div class="home">
         <div class="container-fluid">

            @include('admin.partials.top')

            <div class="row">
               @include('admin.partials.menu')
               <div class="col-md-9">

                 @yield('content')

               </div>
            </div> 
         </div>
      </div>

      {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js') }}
      {{ HTML::script('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js') }}
      {{ HTML::script('js/datatables.js') }}
      {{ HTML::script('js/index.js') }}
    
      <script>
      $(document).ready(function() {
          $('#table-conteudo').DataTable( {
              "pagingType": "simple_numbers",
              "info": false,
              "ordering": false,
              "searching": false,
              "lengthMenu": false
          });
      } );
      </script>
   </body>
</html>