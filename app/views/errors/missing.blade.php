@extends('layouts.base')
@section('content')

<div class="login">
 <img class="logo" src="{{ asset('img/logo-rsa-dark.png') }}">
 <div class="box" style="min-height:255px;">
    <h3>ERRO 404</h3>
    <p>A página que está procurando não foi encontrada.</p>
    <div class="btn-bottom">
       <a href="{{ route('home') }}" class="btn btn-purple">Voltar para a primeira página</a>
    </div>
 </div>
</div>

@stop