<?php

class Franchise extends \Eloquent {
	protected $fillable = ['name'];

	public function posts()
	{
		return $this->belongsToMany('Post', 'franchise_posts');
	}
}