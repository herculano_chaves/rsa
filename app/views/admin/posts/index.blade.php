@extends('admin.base')
@section('content')
@include('layouts.notifications')
{{ Form::open(['route'=>'admin.conteudos.store']) }}
   <div class="combo-add">
      <div class="combo-row">
         <div class="view-type" style="width:80px;"><span class="titulo">Conteúdos</span></div>
         <a href="#" class="btn btn-gray combo-add-new" style="margin:0px 0px 32px 0px;">Novo Conteúdo</a>
         <div class="view-type comp-add">
            <!-- Incluuir formulários aqui  -->
            <div class="form-group">
               {{ Form::label('title','Título') }}
               {{ Form::text('title', null, ['class'=>'form-control']) }}
            </div>
            <div class="form-group">
               {{ Form::label('excerpt','Resumo') }}
               {{ Form::textarea('excerpt', null, ['class'=>'form-control']) }}
            </div>
            <div class="form-group">
               {{ Form::label('content','Conteúdo') }}
               {{ Form::textarea('content', null, ['class'=>'form-control ckeditor']) }}
            </div>
             
         </div>
         <div class="view-btn comp-add"><button type="button" class="btn btn-gray combo-add-cancel">Cancelar</button></div>
         <div class="view-btn comp-add"><button type="submit" class="btn btn-purple">Adicionar</button></div>       
      </div>
   </div>
{{ Form::close() }}

<div class="container-table" align="center">
   <table class="table" >
      @if($posts->count() > 0)
      <thead>
         <tr>
            <th><p class="border-radius-left"></p></th>
            <th style="padding-right:18px;"><p class="border-radius-right">Título</p></th>
            <th><p class="border-radius-left">Resumo</p></th>
            <th>Data</th>
            <th><p></p></th>
            <th><p class="border-radius-right"></p></th>
         </tr>
      </thead>
      <tbody>
         <tr>
            <td><input type="checkbox"></td>
            <td colspan="4"><a href="#" style="margin-left:10px;">Excluir selecionados</a></td>
         </tr>
         @foreach($posts as $post)
            <tr>
               <td><input type="checkbox"></td>
               <td><input class="text" value="{{ $post->title }}" readonly></td>
               <td>{{ $post->excerpt }}</td>
               <td>{{ Helper::ConverterBR($post->created_at) }}</td>
               <td class="btns">
                  <a class="pull-right enabled-text"><i class="fa fa-pencil"></i></a>
                  <a class="pull-right save-text"><i class="fa fa-floppy-o"></i></a>
               </td>
               <td class="btns">
                  <a class="pull-right remove-text"><i class="fa fa-trash-o"></i></a>
                  <a class="pull-right cancel-edit"><i class="fa fa-times"></i></a>
               </td>
            </tr>
         @endforeach
         
      </tbody>
      @else
            <tr>
               <td colspan="4"><i class="fa fa-puzzle-piece"></i> Ainda não temos conteudo para esta área</td>
            </tr>
         @endif
   </table>     
   {{ $posts->links() }}             
</div>
<div style="height:100px;"></div>
@stop