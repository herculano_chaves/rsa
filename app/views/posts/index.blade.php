@section('content')

<!-- slide -->
<div id="sequence" class="seq seq-top">
 <div class="paginacao">
    <ul class="seq-pagination">
       <li><a href="#step1" rel="step1"></a></li>
       <li><a href="#step2" rel="step2"></a></li>
       <li><a href="#step3" rel="step3"></a></li>
    </ul>
 </div>
 <div class="seq-screen">
    <ul class="seq-canvas">
       <li class="step2">
          <div data-seq class="seq-feature seq-half seq-half-right">
             <div style="background-image: url(img/slide-header/img_02.jpg)">
                <img src="img/slide-header/img_02.jpg" />
             </div>
          </div>
          <div data-seq class="seq-content seq-half seq-half-left seq-valign">
             <div class="box">
                <h3>Título de uma matéria em destaque da franquia. Poderá colocar até 3 destaques nesse espaço.</h3>
                <a class="seq-button" href="#" target="_blank">Leia a matéria</a>
             </div>
          </div>
       </li>
       <li class="step3">
          <div data-seq class="seq-feature seq-half seq-half-right">
             <div style="background-image: url(img/slide-header/img_03.jpg)">
                <img src="img/slide-header/img_03.jpg" />
             </div>
          </div>
          <div data-seq class="seq-content seq-half seq-half-left seq-valign">
             <div class="box">
                <h3>Título de uma matéria em destaque da franquia. Poderá colocar até 3 destaques nesse espaço.</h3>
                <a class="seq-button" href="#" target="_blank">Leia a matéria</a>
             </div>
          </div>
       </li>
       <li class="step3">
          <div data-seq class="seq-feature seq-half seq-half-right">
             <div style="background-image: url(img/slide-header/img_03.jpg)">
                <img src="img/slide-header/img_03.jpg" />
             </div>
          </div>
          <div data-seq class="seq-content seq-half seq-half-left seq-valign">
             <div class="box">
                <h3>Título de uma matéria em destaque da franquia. Poderá colocar até 3 destaques nesse espaço.</h3>
                <a class="seq-button" href="#" target="_blank">Leia a matéria</a>
             </div>
          </div>
       </li>

    </ul>
 </div>
</div>
<!-- /slide -->

<div class="container" style="width:100%;">
 <!-- header -->
 <div class="row">
    <div class="col-md-2 padding">
       <img class="logo-rsa" src="img/logo-rsa.png">
    </div>
    <div class="pull-right">
       <div class="col-md-1 menu">
          <img src="img/logo-usuario/logo_subway.png">
       </div>
       <div class="col-md-2 menu">
          <li class="dropdown">
             <a class="dropdown-toggle nome-usuario" data-toggle="dropdown" href="#">Bem-vindo, <span>Fred DeLuca</span><span class="caret"></span></a>
             <ul class="dropdown-menu">
                <li><a href="#"><i class="fa fa-user"></i> Perfil</a></li>
                <li><a href="#"><i class="fa fa-sign-out"></i> Sair</a></li>
             </ul>
          </li>
       </div>
       <div class="col-md-3 menu">
          <div class="btn-group">
             <a class="btn btn-primary">finanças</a>
             <a class="btn btn-primary">Tecnologia</a>
             <a class="btn btn-primary">Mercado</a>
             <a class="btn btn-primary">Comportamento</a>
          </div>
       </div>
       <div class="col-md-1 menu">
          <a href="http://www.rsagroup.com.br/rsa_seguros_institucional" target="_blank" class="btn btn-default">sobre a rsa</a>    
       </div>
    </div>
 </div>
 <!-- <div class="row">
    <div class="col-md-">
    </div>
 </div> -->
 <div class="row padding">
    <div class="col-lg-2 col-md-3 pull-right contatos-rapidos">
       <div class="bg-transparent"></div>
       <h3>Contatos Rápidos</h3>
       <span class="contato cargo">Gerente de Relacionamento</span><br>
       <span class="contato nome">Mara Russi</span><br>
       <span class="contato email">mara.russi@marsh.com </span><br>
              
       <h5>Aviso de Sinistros</h5>
       <span class="email">franquias.subway@marsh.com </span><br>
       <span class="telefone">0800 722 0394 - Opção 2</span><br>

       <h5>Acompanhamento de Sinistros</h5>
       <span class="email">talita.alves@marsh.com </span><br>
       <span class="telefone">0800 722 0394 - Opção 3</span><br>
       <span>Atendimento de segunda à Sexta-Feira, das 08h30min às 17h </span><br>
       
       <h5>Assistência RSA 24 horas</h5>
       <span class="telefone">3003-7727 (capitais e Regiões metropolitanas) </span><br>
       <span class="telefone">0800 704 9399 (demais Regiões) </span><br>
       <span>Atendimento 24 horas, todos os dias da semana.</span><br>

    </div>
 </div>
 <!-- /header -->
 <div style="margin-bottom:30px;"></div>

 <div class="row">
    <!-- coluna principal -->
    <div class="col-md-9">
       <div class="row">
          <div class="col-md-4">
             <div class="img-info">
                <img src="img/info1.png"/>
                <div class="box-text">
                   <p>Informações importantes sobre o seu seguro, planos e principais coberturas</p>
                   <a class="btn btn-white">veja mais</a>
                </div>
             </div>
          </div>
          <div class="col-md-4">
             <div class="img-info">
                <img src="img/info2.png"/>
                <div class="box-text">
                   <p>Passo-a-passo de como reportar um sinistro</p>
                   <a class="btn btn-white">veja mais</a>
                </div>
             </div>
          </div>
          <div class="col-md-4">
             <div class="img-info">
                <img src="img/info2.png"/>
                <div class="box-text">
                   <p>ASSISTÊNCIA 24 – Confira a lista de Assistências 24 horas</p>
                   <a class="btn btn-white">veja mais</a>
                </div>
             </div>
          </div>
       </div>
       <div style="padding-top:60px;"></div>
       <div class="row">
          <div class="col-md-12">
             <div id="carousel-text" class="carousel slide carousel-home-logado" data-ride="carousel">
                <div class="row">
                   <div class="col-md-5 col-md-push-7">
                      <ol class="carousel-indicators">
                         <li data-target="#carousel-text" data-slide-to="0" class="active"></li>
                         <li data-target="#carousel-text" data-slide-to="1"></li>
                         <li data-target="#carousel-text" data-slide-to="2"></li>
                      </ol>
                   </div>
                </div>
                <div class="row">
                <div class="carousel-inner">
                   <div class="item active">
                      <div class="col-md-7">
                          <iframe class="video" src="https://www.youtube.com/embed/yeOQ7ktDoDk" frameborder="0" allowfullscreen></iframe>
                      </div>
                      <div class="col-md-5">
                         <div class="carousel-content">
                            <div class="carousel-box-text">
                               <h3>A prática cotidiana prova que o desafiador cenário globalizado estimula a padronização da gestão inovadora da qual fazemos parte</h3>
                               <br>
                               <p>Caros amigos, o novo modelo estrutural aqui preconizado assume importantes posições no estabelecimento das condições inegavelmente apropriadas.</p>
                               <br>
                               <p>A certificação de metodologias que nos auxiliam a lidar com a contínua expansão de nossa atividade faz parte de um processo de gerenciamento das posturas dos órgãos dirigentes com relação às suas atribuições.</p>
                               <br>
                               <p class="data">comportamento  |  12 abril 2015 - 16:10</p>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="item">
                      <div class="col-md-7">
                          <img src="img/slide-header/img_02.jpg" class="video">
                      </div>
                      <div class="col-md-5">
                         <div class="carousel-content">
                            <div class="carousel-box-text">
                               <h3>A prática cotidiana prova que o desafiador cenário globalizado estimula a padronização da gestão inovadora da qual fazemos parte</h3>
                               <br>
                               <p>Caros amigos, o novo modelo estrutural aqui preconizado assume importantes posições no estabelecimento das condições inegavelmente apropriadas.</p>
                               <br>
                               <p>A certificação de metodologias que nos auxiliam a lidar com a contínua expansão de nossa atividade faz parte de um processo de gerenciamento das posturas dos órgãos dirigentes com relação às suas atribuições.</p>
                               <br>
                               <p class="data">comportamento  |  12 abril 2015 - 16:10</p>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="item">
                      <div class="col-md-7">
                          <img src="img/slide-header/img_03.jpg" class="video">
                      </div>
                      <div class="col-md-5">
                         <div class="carousel-content">
                            <div class="carousel-box-text">
                               <h3>A prática cotidiana prova que o desafiador cenário globalizado estimula a padronização da gestão inovadora da qual fazemos parte</h3>
                               <br>
                               <p>Caros amigos, o novo modelo estrutural aqui preconizado assume importantes posições no estabelecimento das condições inegavelmente apropriadas.</p>
                               <br>
                               <p>A certificação de metodologias que nos auxiliam a lidar com a contínua expansão de nossa atividade faz parte de um processo de gerenciamento das posturas dos órgãos dirigentes com relação às suas atribuições.</p>
                               <br>
                               <p class="data">comportamento  |  12 abril 2015 - 16:10</p>
                            </div>
                         </div>
                      </div>
                   </div>

                </div>
             </div>
          </div>
          <!-- /carrousel -->
       </div>
    </div>
    <!-- /coluna principal -->
 </div>

    <!-- coluna lateral -->
    <div class="col-md-3">
       <h3>Artigos relevantes para o seu negócio, com sugestões de gestão e marketing</h3>
       <br>
       <p>Por outro lado, a consulta aos diversos militantes faz parte de um processo de gerenciamento das posturas dos órgãos dirigentes com relação às suas atribuições.</p>

       <div style="margin-top:40px;"></div>

       <ul class="news">
          <li>
             <a href="#">
                <div class="row">
                   <div class="col-md-4">
                      <img src="img/news/post2.png">
                   </div>
                   <div class="col-md-8">
                      <h5>complexidade dos estudos efetuados desafia a capacidade</h5>
                      <p>Evidentemente, a competitividade nas transações comerciais apresenta tendências...</p>
                      <p class='dados'>Mercado  |  12:36  |  25 agosto 2015</p>
                   </div>
                </div>
             </a>
          </li>
          <li>
             <a href="#">
                <div class="row">
                   <div class="col-md-4">
                      <img src="img/news/post1.png">
                   </div>
                   <div class="col-md-8">
                      <h5>revolução dos costumes estende o alcance e a importância</h5>
                      <p>Evidentemente, a competitividade nas transações comerciais apresenta tendências...</p>
                      <p class='dados'>finanças  |  05 agosto 2015 - 15:36</p>
                   </div>
                </div>
             </a>
          </li>
          <li>
             <a href="#">
                <div class="row">
                   <div class="col-md-4">
                      <img src="img/news/post3.png">
                   </div>
                   <div class="col-md-8">
                      <h5>complexidade dos estudos efetuados desafia a capacidade</h5>
                      <p>Evidentemente, a competitividade nas transações comerciais apresenta tendências...</p>
                      <p class='dados'>finanças  |  05 agosto 2015 - 15:36</p>
                   </div>
                </div>
             </a>
          </li>
          <li>
             <a href="#">
                <div class="row">
                   <div class="col-md-4">
                      <img src="img/news/post4.png">
                   </div>
                   <div class="col-md-8">
                      <h5>complexidade dos estudos efetuados desafia a capacidade</h5>
                      <p>Evidentemente, a competitividade nas transações comerciais apresenta tendências...</p>
                      <p class='dados'>finanças  |  05 agosto 2015 - 15:36</p>
                   </div>
                </div>
             </a>
          </li>
          <li>
             <a href="#">
                <div class="row">
                   <div class="col-md-4">
                      <img src="img/news/post5.png">
                   </div>
                   <div class="col-md-8">
                      <h5>complexidade dos estudos efetuados desafia a capacidade</h5>
                      <p>Evidentemente, a competitividade nas transações comerciais apresenta tendências...</p>
                      <p class='dados'>finanças  |  05 agosto 2015 - 15:36</p>
                   </div>
                </div>
             </a>
          </li>
       </ul>

    </div>
    <!-- /coluna lateral -->

 </div>

 <div style="margin-top:100px;"></div>
 
@stop