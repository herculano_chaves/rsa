@section('content')

<div id="sequence" class="seq" data-scroll="true">

<div class="paginacao">
  <ul class="seq-pagination">      
    <li><a href="#step1" rel="step1"></a></li>
    <li><a href="#step2" rel="step2"></a></li>
    <li><a href="#step3" rel="step3"></a></li>
    <li><a href="#step4" rel="step4"></a></li>
    <li><a href="#step5" rel="step5"></a></li>
    <li><a href="#step6" rel="step6"></a></li>
    <li><a href="#step7" rel="step7"></a></li>
    <li><a href="#step8" rel="step8"></a></li>
  </ul>  
</div>

  <div class="seq-screen"> 
    <ul class="seq-canvas">
      <li class="step1 seq-in">
        <div data-seq class="seq-feature seq-half">
          <div style="background-image: url(img/slide-header/img_01.jpg)">
            <img src="img/slide-header/img_01.jpg" alt="A close-up of green fern leaves" />
          </div>
        </div>
        <div data-seq class="seq-content seq-half seq-valign">
          <div class="box">
            <h3>A RSA Seguros é especialista em frotas corporativas e possui soluções específicas para empresas.</h3>
            <a class="seq-button" href="http://www.rsagroup.com.br/seguro_auto_frotas_grandes_riscos" target="_blank">Veja as vantagens</a>
          </div>
        </div>
      </li>
      <li class="step2">
        <div data-seq class="seq-feature seq-half seq-half-right">
          <div style="background-image: url(img/slide-header/img_02.jpg)">
            <img src="img/slide-header/img_02.jpg" alt="A close-up of green fern leaves" />
          </div>
        </div>
        <div data-seq class="seq-content seq-half seq-half-left seq-valign">
          <div class="box">
            <h3>A RSA Seguros oferece os seguros Vida em Grupo para pequenas e médias empresas, de três a 500 funcionários.</h3>
            <a class="seq-button" href="http://www.rsagroup.com.br/seguros_de_vida" target="_blank">Veja as vantagens</a>
          </div>
        </div>
      </li>
      <li class="step3">
        <div data-seq class="seq-feature seq-half seq-half-right">
          <div style="background-image: url(img/slide-header/img_03.jpg)">
            <img src="img/slide-header/img_03.jpg" alt="A close-up of green fern leaves" />
          </div>
        </div>
        <div data-seq class="seq-content seq-half seq-half-left seq-valign">
          <div class="box">
            <h3>A RSA Seguros oferece serviços de consultoria em Gerenciamento de Riscos adaptados às necessidades de cada cliente.</h3>
            <a class="seq-button" href="http://www.rsagroup.com.br/seguro_gerenciamento_riscos" target="_blank">Veja as vantagens</a>
          </div>
        </div>
      </li>
      <li class="step4">
        <div data-seq class="seq-feature seq-half seq-half-right">
          <div style="background-image: url(img/slide-header/img_04.jpg)">
            <img src="img/slide-header/img_04.jpg" alt="A close-up of green fern leaves" />
          </div>
        </div>
        <div data-seq class="seq-content seq-half seq-half-left seq-valign">
          <div class="box">
            <h3>O Seguro Residencial tem mais de 15 coberturas, amplos limites e diversos serviços que facilitam o seu dia a dia.</h3>
            <a class="seq-button" href="http://www.rsagroup.com.br/seguro_residencial" target="_blank">Veja as vantagens</a>
          </div>
        </div>
      </li>
      <li class="step5">
        <div data-seq class="seq-feature seq-half seq-half-right">
          <div style="background-image: url(img/slide-header/img_05.jpg)">
            <img src="img/slide-header/img_05.jpg" alt="A close-up of green fern leaves" />
          </div>
        </div>
        <div data-seq class="seq-content seq-half seq-half-left seq-valign">
          <div class="box">
            <h3>A RSA Seguros é especialista em frotas corporativas e possui soluções específicas para empresas. </h3>
            <a class="seq-button" href="http://www.rsagroup.com.br/seguro_auto_frotas_grandes_riscos" target="_blank">Veja as vantagens</a>
          </div>
        </div>
      </li>
      <li class="step6">
        <div data-seq class="seq-feature seq-half seq-half-right">
          <div style="background-image: url(img/slide-header/img_06.jpg)">
            <img src="img/slide-header/img_06.jpg" alt="A close-up of green fern leaves" />
          </div>
        </div>
        <div data-seq class="seq-content seq-half seq-half-left seq-valign">
          <div class="box">
            <h3>A RSA Seguros oferece os seguros Vida em Grupo para pequenas e médias empresas, de três a 500 funcionários.</h3>
            <a class="seq-button" href="http://www.rsagroup.com.br/seguros_de_vida" target="_blank">Veja as vantagens</a>
          </div>
        </div>
      </li>
      <li class="step7">
        <div data-seq class="seq-feature seq-half seq-half-right">
          <div style="background-image: url(img/slide-header/img_07.jpg)">
            <img src="img/slide-header/img_07.jpg" alt="A close-up of green fern leaves" />
          </div>
        </div>
        <div data-seq class="seq-content seq-half seq-half-left seq-valign">
          <div class="box">
            <h3>A RSA Seguros oferece serviços de consultoria em Gerenciamento de Riscos adaptados às necessidades de cada cliente.</h3>
            <a class="seq-button" href="http://www.rsagroup.com.br/seguro_gerenciamento_riscos" target="_blank">Veja as vantagens</a>
          </div>
        </div>
      </li>
      <li class="step8">
        <div data-seq class="seq-feature seq-half seq-half-right">
          <div style="background-image: url(img/slide-header/img_08.jpg)">
            <img src="img/slide-header/img_08.jpg" alt="A close-up of green fern leaves" />
          </div>
        </div>
        <div data-seq class="seq-content seq-half seq-half-left seq-valign">
          <div class="box">
            <h3>O Seguro Residencial tem mais de 15 coberturas, amplos limites e diversos serviços que facilitam o seu dia a dia.</h3>
            <a class="seq-button" href="http://www.rsagroup.com.br/seguro_residencial" target="_blank">Veja as vantagens</a>
          </div>
        </div>
      </li>
    </ul>
  </div>

</div>
@stop