<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>RSA Franquias @yield('title')</title>
  <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">

  {{ HTML::style('css/sequence-theme.two-up.css') }}
  {{ HTML::style('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css') }}
  {{ HTML::style('css/font-awesome/css/font-awesome.min.css') }}
  {{ HTML::style('css/index.css') }}

  <!--[if lt IE 9]>
    {{ HTML::style('css/sequence-theme.two-up-ie8.css') }}
  <![endif]-->
</head>
<body class="bg-gray admin">
  
  {{ $header or null }}

  @yield('content')

  {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js') }}
  {{ HTML::script('http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js') }}
  {{ HTML::script('scripts/imagesloaded.pkgd.min.js') }}
  {{ HTML::script('scripts/hammer.min.js') }}
  {{ HTML::script('scripts/sequence.min.js') }}
  {{ HTML::script('scripts/sequence-theme.two-up.js') }}
  
</body>
</html>
