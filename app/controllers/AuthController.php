<?php

class AuthController extends \BaseController {
	protected $layout = 'layouts.base';

	public function register()
	{
		$states = State::orderBy('name')->lists('name','id');
		
		$cities = array();

		if(Input::old('state_id')){
			$cities = City::whereState_id(Input::old('state_id'))->orderBy('name')->lists('name','id');
		}  
		
		$this->layout->content = View::make('users.register', compact('states','cities'));
	}

	public function postRegister()
	{
		$data = Input::all();

		$validator = Validator::make($data, User::$rules);


		if($validator->fails()){
			return Redirect::route('user.register')->withErrors($validator)->withInput(Input::except('src'));
		}

		$u = Sentry::register([
			'email' => $data['email'],
			'password'=>$data['password'],
			'name' => $data['name']
		]);

		$user = User::find($u->id);

		// Criando userdata
		$userdata = new Userdata;
		$userdata->city_id = $data['city_id'];

		// Subido arquivo de imagem Base64
		if(Input::has('src') ){

			if(!base64_decode(Input::get('src'))){
				return false;
			}

			$full = 'uploads/'.uniqid().'.jpg';

			$src = $this->base64_to_jpeg($data['src'], $full);

			unlink($full);
			
			if(File::exists($src)){
				$image = $this->upload_file($src);
				$userdata->src = $image;
				$userdata->user()->associate($user);
				
			}
			
		}

		// Salvando userdata
		$userdata->save();

		$group = Sentry::findGroupById($data['group_id']);

		$u->addGroup($group);

		$activationCode = $u->getActivationCode();
		
	    // Send activation code to the user so he can activate the account
	    Mail::send('emails.user.register', array('activationCode'=>$activationCode, 'user'=>$user), function($message){

			$message->to(Input::get('email'),Input::get('name'))->subject('Confirmação de cadastro');

		});

		Session::set('WaitingRegisterConfirm',1);

		return Redirect::route('user.register.return')->withSuccess(Lang::get('user.register.success'));
	}

	public function registerReturn()
	{
		if(!Session::has('WaitingRegisterConfirm')){
			return Redirect::route('user.register');
		}
		$this->layout->content = View::make('users.return');
	}

	public function activation($activationCode)
	{
		try
		{
		    $user = Sentry::findUserByActivationCode($activationCode);

		    if ($user->attemptActivation($activationCode)){

		    	// Deletando a sessão de aguardando confirmação de registro
		    	Session::forget('WaitingRegisterConfirm');

		    	return Redirect::route('user.login')->withSuccess(Lang::get('user.activated'));
		    }
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			return Redirect::route('user.register')->withError(Lang::get('user.notfound'));
		}
	}

	public function login()
	{
		$this->layout->content = View::make('users.login');
	}

	public function postLogin()
	{
		return $this->masterLogin('user.login', 'user.profile');
	}

	public function logout()
	{
		return $this->masterLogout('home');
	}

	/*
	* Login Administrativo
	* 
	*/
	public function adminLogin()
	{	
		$this->layout->content = View::make('admin.index');
	}

	public function postAdminLogin()
	{
		return $this->masterLogin('admin.login', 'admin.dashboard');
	}

	public function adminLogout()
	{
		return $this->masterLogout('admin.login');
	}


}