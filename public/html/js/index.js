$(document).ready(function(){
	// Carousel com scroll
	$('.home #carousel-home').bind('mousewheel', function(e){
	    if(e.originalEvent.wheelDelta /120 > 0) {
	        $(this).carousel('next');
	    }
	    else{
	        $(this).carousel('prev');
	    }
	});

	// Contador pagina home admin
	$('.count').each(function () {
	    $(this).prop('Counter',0).animate({
	        Counter: $(this).text()
	    }, {
	        duration: 4000,
	        easing: 'swing',
	        step: function (now) {
	            $(this).text(Math.ceil(now));
	        }
	    });
	});

	// Combo para adicionar linha a tabela
	$('.combo-add .combo-add-new').click(function(){
		$(this).hide();
		$('.comp-add').fadeTo("fast",1);
	});
	$('.combo-add .combo-add-cancel').click(function(){
		$('.comp-add').css('opacity','0');
		$('.combo-add-new').fadeTo("fast",1);
	});

	// Habilita a edição do texto da tabela
	$('table .enabled-text').click(function(){
		$input = $(this).parents('tr').find('.text');
		$input.attr('readonly', false);
		$(this).parents('tr').find('.enabled-text, .remove-text').hide();
		$(this).parents('tr').find('.save-text, .cancel-edit').show();
		$(this).parents('tr').append('<input type="hidden" value="'+$input.val()+'">');
	});

	// Desabilita a edição do texto da tabela
	$('table .cancel-edit').click(function(){
		$input = $(this).parents('tr').find('.text');
		$input.attr('readonly', true).val( $(this).parents('tr').find('input[type="hidden"]').val() );
		$(this).parents('tr').find('input[type="hidden"]').remove();
		$(this).parents('tr').find('.enabled-text, .remove-text').show();
		$(this).parents('tr').find('.save-text, .cancel-edit').hide();
	});

	// Confirma exclusão da linha da tabela
	$('table#table-conteudo .remove-text').click(function(){
		$tds = $(this).parents('tr').find('td');
		$qtdTds = $tds.size() - 4;
		$newTds = "";
		for($i=0; $i<$qtdTds; $i++){
			$newTds += '<td class="delete-row-table"></td>';
		}
		$tds.hide();
		$(this).parents('tr').append('<td class="delete-row-table"></td><td class="delete-row-table">Você tem certeza que deseja excluir este item?</td><td class="delete-row-table"><a class="btn-delete-row-table">Excluir</a></td>'+$newTds+'<td class="delete-row-table"><a onclick="cancelDelRowTable(this)"><i class="fa fa-times pull-right"></i></a></td>');
	});
});

// Cancela exclusão da linha da tabela
function cancelDelRowTable(e){
	$(e).parents('tr').find('td').show();
	$(e).parents('tr').find('.delete-row-table').remove();
}