<?php 

return array(

	'create' => array(
		'success' => ':element criado com sucesso'
	),
	'destroy' => array(
		'success' => ':element apagado'
	),
	'update' => array(
		'success' => ':element editado com sucesso'
	),
);