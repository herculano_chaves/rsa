@if($message = Session::get('success'))
	<div class="alert alert-success">
		<p>{{ $message }}</p>
	</div>
@endif

@if($message = Session::get('error'))
	<div class="alert alert-danger">
		<p>{{ $message }}</p>
	</div>
@endif
@if($errors->count()>0)
	<ul class="alert alert-danger">
	@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
	</ul> 
@endif
{{-- encerrando sessão de aviso --}}