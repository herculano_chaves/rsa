@section('content')

<div class="login">
 <img class="logo" src="../img/logo-rsa-dark.png">
 <div class="box">
    <h3>Administração do sitema</h3>
    @include('layouts.notifications')
	{{ Form::open(['route'=>'admin.login']) }}
       <div class="form-group">
         <input type="email" name="email" value="{{ Input::old('email') }}" class="form-control" placeholder="E-mail">
       </div>
       <div class="form-group">
         <input type="password" name="password" class="form-control" placeholder="Senha">
       </div>
       <button type="submit" class="pull-right">Entrar</button>
       <div class="checkbox">
          <label><input type="checkbox">Salvar senha</label>
       </div>
       <a href="{{ route('password.remind') }}">Esqueceu sua senha?</a>
   	{{ Form::close() }}
 </div>
</div>

@stop