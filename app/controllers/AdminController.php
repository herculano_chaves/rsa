<?php

class AdminController extends \BaseController {

	protected $layout = 'admin.base';

	public function dashboard()
	{
		$this->layout->content = View::make('admin.dashboard');
	}
}