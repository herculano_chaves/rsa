<?php
/* Rotas da aplicação
* 
*/

Route::get('/', ['as'=>'home', 'uses'=>'SiteController@home']);

# User Register
Route::get('cadastro',['before'=>'already.logged','as'=>'user.register','uses'=>'AuthController@register']);
Route::post('cadastro',['as'=>'user.post.register','uses'=>'AuthController@postRegister']);
Route::get('cadastro/confirmado',['as'=>'user.register.return','uses'=>'AuthController@registerReturn']);
Route::get('cadastro/confirmacao/{activationCode}', ['as'=>'user.register.activation', 'uses'=>'AuthController@activation']);
Route::get('login',['as'=>'user.login','uses'=>'AuthController@login']);
Route::post('login',['as'=>'user.post.login','uses'=>'AuthController@postLogin']);
Route::get('logout',['as'=>'user.logout','uses'=>'AuthController@logout']);

# Passwords
Route::get('recuperar/senha', ['uses' => 'PasswordController@remind',  'as' => 'password.remind']);
Route::post('password/reset', ['uses' => 'PasswordController@request', 'as' => 'password.request']);
Route::get('resetar/senha/{token}', ['uses' => 'PasswordController@reset', 'as' => 'password.reset']);
Route::post('password/reset/{token}', ['uses' => 'PasswordController@update', 'as' => 'password.update']);

# Admin
Route::get('admin', ['before'=>'already.logged.admin', 'as'=>'admin.login', 'uses'=>'AuthController@adminLogin']);
Route::post('admin', ['as'=>'admin.post.login', 'uses'=>'AuthController@postAdminLogin']);

Route::group(['prefix'=>'admin', 'before'=>'auth|admin'], function(){
	# Dashoboard
	Route::get('dashboard', ['as'=>'admin.dashboard', 'uses'=>'AdminController@dashboard']);

	# Posts / Conteúdos
	Route::resource('conteudos', 'PostsController');

	# Categorias
	Route::resource('categorias', 'CategoriesController');

	# Logout
	Route::get('logout', ['as'=>'admin.logout', 'uses'=>'AuthController@adminLogout']);
});

