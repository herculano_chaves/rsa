@section('content')

<span class="titulo">Relatórios</span>
<a href="#" class="btn btn-gray" style="margin:0px 0px 20px 10px;">Visualizar todos</a>
<div class="row">
 <div class="col-md-4">
    <div class="box">
       <p class="count">89</p>
       <div class="box-label">Total de Franquias</div>
    </div>
 </div>
 <div class="col-md-4">
    <div class="box">
       <p class="count">2245</p>
       <div class="box-label">Total de Usuários</div>
    </div>
 </div>
 <div class="col-md-4">
    <div class="box">
       <p class="count">9346</p>
       <div class="box-label">Total de visitantes no mês</div>
    </div>
 </div>
</div>
<div class="hr"></div>

<p class="titulo">Conteúdos</p>
<div class="row">
 <div class="col-md-4">
    <div class="box">
       <a class="btn btn-purple btn-block">Novo conteúdo</a>
       <a class="btn btn-gray btn-block">Visualizar todo</a>
    </div>
 </div>
 <div class="col-md-4">
    <div class="box">
       <a class="btn btn-purple btn-block">Nova categoria</a>
       <a class="btn btn-gray btn-block">Visualizar categorias</a>
    </div>
 </div>
 <div class="col-md-4">
    <div class="box">
       <a class="btn btn-purple btn-block">Nova mídia</a>
       <a class="btn btn-gray btn-block">Visualizar mídias</a>
    </div>
 </div>
</div>
<div class="hr"></div>


<div class="row">
 <div class="col-md-4">
    <p class="titulo">Franquias</p>
    <div class="box">
       <a class="btn btn-purple btn-block">Nova franquia</a>
       <a class="btn btn-gray btn-block">Visualizar franquias</a>
    </div>
 </div>
 <div class="col-md-4">
    <p class="titulo">FAQs</p>
    <div class="box">
       <a class="btn btn-purple btn-block">Novo FAQ</a>
       <a class="btn btn-gray btn-block">Visualizar FAQs</a>
    </div>
 </div>
 <div class="col-md-4">
    <p class="titulo">Links externos</p>
    <div class="box">
       <a class="btn btn-purple btn-block">Novo link externo</a>
       <a class="btn btn-gray btn-block">Visualizar links externos</a>
    </div>
 </div>
</div>
<div style="margin-bottom:20px;"></div>

@stop