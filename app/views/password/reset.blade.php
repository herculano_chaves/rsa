@section('content')

<div class="login">
 <img class="logo" src="../img/logo-rsa-dark.png">
 <div class="box" style="min-height:255px;">
    <h3>Redefinir senha</h3>
    {{ Form::open(['route'=>array('password.reset', $token)]) }}
	{{ Form::hidden('token', $token) }}
       <div class="form-group">
         <input type="password" name="password" class="form-control" placeholder="Digite sua nova senha (min. 6 caracteres)">
       </div>
       <div class="form-group">
         <input type="password" name="password_confirmation" class="form-control" placeholder="Confirmar nova senha">
       </div>
       <div style="width:100%; display:inline-block;">
          <button type="submit" class="pull-right">Salvar nova senha</button>
       </div>
    {{ Form::close() }} <!-- /form -->
 </div>
</div>
      
@stop