<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Passwords must be at least six characters and match the confirmation.",

	"user" => "We can't find a user with that e-mail address.",

	"token" => "This password reset token is invalid.",

	"sent" => "Sua solicitação de recadastramento de senha foi realizada com sucesso!
        Acesse seu e-mail cadastrado e clique no link enviado para redefini-la. ",

	"error" => "E-mail não cadastrado. Favor inserir um e-mail válido."

);
