<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>RSA Franquias</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<table width="600" align="center" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td><img src="{{ asset('img/news/password/NewsRecuperarSenha_01.jpg') }}" style="display:block; border:none;"></td>
		</tr>
		<tr>
			<td>
				<table width="600" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td><img src="{{ asset('img/news/password/NewsRecuperarSenha_02.jpg') }}" style="display:block; border:none;"></td>
						<td width="497" height="144">
							<font face="Arial" size="2" color="#3c3d3d" style="line-height:25px;">
								<b>Olá, {{ $user->name }}.</b> <br>
								Você solicitou uma nova senha para a administração do sistema. <br>
								Clique no link a seguir e será direcionado para a criação de uma nova senha. <br>
								
								<div style="word-wrap: break-word; width:497px; margin-top:10px;">
									<a href="{{ route('password.reset', $resetCode) }}" target="_blank" style="color:#bc237f;">{{ route('password.reset', $resetCode) }}</a>
								</div>
							</font>
						</td>
						<td><img src="{{ asset('img/news/password/NewsRecuperarSenha_04.jpg') }}" style="display:block; border:none;"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><img src="{{ asset('img/news/password/NewsRecuperarSenha_05.jpg') }}" style="display:block; border:none;"></td>
		</tr>
	</table>
</body>
</html>
