<?php

class SiteController extends BaseController {

	protected $layout = 'layouts.base';

	public function home()
	{
		$this->layout->header = View::make('layouts.partials.header');
		$this->layout->content = View::make('site.home');
	}

}
