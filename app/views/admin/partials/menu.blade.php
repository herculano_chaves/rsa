{{-- MENU --}}
<div class="col-md-3">
   <div class="menu-lateral">
      <ul>
         <li>
            <a href="{{ route('admin.conteudos.index') }}">Conteúdos</a>
            <ul>
               <li>
                  <a href="{{ route('admin.categorias.index') }}">Categorias</a>
               </li>
               <li>
                  <a href="#">Vídeos</a>
               </li>
            </ul>
         </li>
         <li>
            <a href="#">Franquia</a>
         </li>
         <li>
            <a href="#">Corretora</a>
         </li>
         <li>
            <a href="#">FAQs</a>
         </li>
         <li>
            <a href="#">Externos</a>
         </li>
         <li>
            <a href="#">Relatórios</a>
         </li>
      </ul>
   </div>
</div>