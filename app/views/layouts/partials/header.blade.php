<div class="container header home padding" style="width:100%;">
  <div class="row">
    <div class="col-md-4 col-sm-12">
      <img src="{{ asset('img/logo-rsa.png') }}">
    </div>
    {{ Form::open(['route'=>'user.login']) }}
      <div class="col-md-2 col-sm-3">
        <input type="text" class="form-control valign" placeholder="login">
      </div>
      <div class="col-md-2 col-sm-3">
        <div class="input-icon icon-right">
          <input type="text" class="form-control valign" placeholder="senha">
          <button type="submit"><i class="fa fa-arrow-right"></i></button>
          <a href="{{ route('password.remind') }}" class="esqueceu-senha">Esqueceu sua senha?</a>
        </div>
      </div> 
    {{ Form::close() }}
    <div class="col-md-2 col-sm-3">
      <a href="http://www.rsagroup.com.br/rsa_seguros_institucional" target="_blank" class="btn btn-default pull-right valign">sobre a rsa</a>    
    </div> 
    <div class="col-md-2 col-sm-3">  
      <a href="http://www.rsagroup.com.br/franquias" target="_blank" class="btn btn-default valign">Quero ser franqueado</a> 
    </div> 
  </div>
</div>