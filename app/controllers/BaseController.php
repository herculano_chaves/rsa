<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	public function masterLogin($input_route, $output_route){

		$credentials = ['email'=>Input::get('email'), 'password'=>Input::get('password')];

		$validator = Validator::make($credentials, ['email'=>'required|email','password'=>'required']);

		if($validator->fails()){
			return Redirect::route($input_route)->withInput()->withErrors($validator);
		}

		try{
			//logando o usuario e redirecionando
			Sentry::authenticate(Input::only('email', 'password'), Input::get('remember-me', 0));

			return Redirect::route($output_route);
		}
		catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
		{
			return Redirect::route($input_route)->with('error','Usuário não encontrado')->withInput(Input::except('password'));
		}
		catch (Cartalyst\Sentry\Users\UserNotActivatedException $e)
		{
			return Redirect::route($input_route)->with('error','Usuário não ativado!')->withInput(Input::except('password'));
		}
	}

	public function masterLogout($route){
		Sentry::logout();

		return Redirect::route($route);

	}

	public function base64_to_jpeg($base64_string, $output_file) {
		    $ifp = fopen($output_file, "wb"); 

		    $data = explode(',', $base64_string);

		    fwrite($ifp, base64_decode($data[1])); 
		    fclose($ifp); 

		    return $output_file; 
	}

	public function upload_file($file, $w=470, $h=226, $thumbs=true, $filename=null){

		$destinationPath = public_path() . '/uploads/';
		

        $ext = $file->getClientOriginalExtension();
        $name = uniqid(rand());
        $filename = $name.'.'.$ext;

        $upload_success = $file->move($destinationPath, $filename);
        
        if ($upload_success) {
            
            // resizing an uploaded file
            $destinationThumbPath = $destinationPath.'thumbs/';
            $destinationNormalPath = $destinationPath.'normal/';
                 
            Image::make($destinationPath . $filename)
            ->fit($w,$h, function($constraint){$constraint->upsize();})
            ->save($destinationNormalPath . $filename);

            if($thumbs == true){
            	 Image::make($destinationPath . $filename)
	            ->fit(160,160)
	            ->save($destinationThumbPath . $filename);
            }
      
            File::delete($destinationPath.$filename);
	       
            return $filename;
        } 
        
        return false;
	}
	
	public function getUser(){
		$u = Sentry::getUser();
		$user = User::find($u->id);
		return $user;
	}


}
