<?php 

return array(
	'empty' => 'Ainda não temos conteúdo nesta área',
	'dualUrl' => '<p class="alert alert-warning">Sua URL está duplicada, favor alterar para evitar conflito com outras</p>',
	'contact' => 'Mensagem enviada com sucesso, em breve entraremos em contato.'
);