<div class="header-admin">
   <div class="row">
      <div class="col-md-6">
         <img class="logo" src="{{ asset('img/logo-rsa-dark.png') }}">
      </div>
      <div class="col-md-6">
         <select class="actions_admin_select select-menu pull-right">
            <option disabled selected>{{ Sentry::getUser()->name }}</option>
            <option value="{{-- route('user.edit') --}}">Meus dados</option>
            <option value="{{ route('admin.logout') }}">Sair</option>
         </select>
      </div>
   </div>
</div>